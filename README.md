# NAME

vim-deadc0de

# VERSION

0.2.1

# DESCRIPTION

Hex editor for Vim.
It uses UNIX utility xxd for conversion.

# FEATURES

- Open hex editor by pressing \<Shift-F6\>. (Of course, you can set your own
  keybinding.) It does buffer conversion.
- When you complete editing, convert to plain text/blob back by pressing
  \<Shift-F6\>.
- Vim almost supports hex edition, but it lacks of very important features.
  This plugin updates ASCII block of edited blob while you're changing the
  HEXes.
- Reverse action is supported too. Edit the ASCII block and it changes the
  HEX.
  But be sure the edited ASCII line doesn't contain any unprintable
  characters, i. e. control sequences. Such characters are presented by
  dotas (.).
- The plugin highlights the currently selected byte in HEX block and the
  corresponding character in ASCII block.
- Reverse is done too.
- Switch between them by entering "gs". (Again, you can set your own
  keybinding.)
- Data protection prevents you from breaking a hex file.
  Assume you has entered "x" character, which is not hexadecimal character.
  Buffer would be restored immidiately.
- Data protection can be toggled by "gp". (Or another keybinding.)
- xxd utility supports only 7-bit ASCII table encoding that outputs in ASCII
  block. (Not all special characters work even so.).
  Extended encodings which use eight bit are not supported by xxd.
  But this plugin adds support for all printable characters including eight bit.
  (One-byte encodings Latin 1, KOI8-R, CP1251, CP866 are tested; One-byte
  characters from UTF-8 are tested.)
- Work with newlines accurately.
  Please, set binary mode and turn off EOL if you are editing an blob.
- Open hexadecimal editor in a new buffer. (Due to safety.)
  The buffer is saved to temporary directory, usually, /tmp/.
- Some options allow to tune the plugin. (Number of them will increase in
  future.)

# SCREENSHOTS

![Alt Text](screenshot.png "vim-deadc0de screenshot")

# SCREENCAST

TODO

# INSTALL

INSTALL file describes how to install the vim-deadc0de.

# USAGE

Now you can open any file in your Vim.
If you're editing an blob, be sure binary mode is enabled.
Also turn off adding of end-of-line characters.

    :set binary
    :set noendofline

Vim adds newline character if you hadn't set this option.
Set proper encoding:

    :edit ++encoding=koi8-r " Replace encoding by yours

Now press \<Shift-F6\>.
Edit hex and convert it back: \<Shift-F6\>. Save.

RTFM if you still have any questions:

:help vim-deadc0de

# FOR HACKERS

See TODO.

Disable extended conversion if file is too big and you don't want to wait. You
won't see any characters differ from latin in this case.
Add the setting below to your .vimrc:

    let g:deadc0de_ascii_extended = 0

You may change HEX bytes case to uppercase:

    let g:deadc0de_hex_uppercase = 1

Disable all filetype plugins including this one by writing:

    let b:did_ftplugin = 0

Additionally, you can setup your encoding smartly:

    " General behavior
    set wildcharm=<Tab> " Command-line completion key

    " Encodings order
    set encoding=utf-8
    set termencoding=utf-8
    set fileencodings=utf-8,koi8-r,cp866,cp1251,ucs-2le,latin1

    " <F8> Change file format (UNIX / DOS / Mac)
    menu File\ format.UNIX :set fileformat=unix<CR>
    menu File\ format.DOS  :set fileformat=dos<CR>
    menu File\ format.Mac  :set fileformat=mac<CR>
    noremap <F8> :emenu File\ format.<Tab>
    imap    <F8> <C-O><F8>

    " <Ctrl-F8> Change open encoding
    menu Encoding.UTF-8   :edit ++encoding=utf-8<CR>
    menu Encoding.KOI8-R  :edit ++encoding=koi8-r<CR>
    menu Encoding.CP866   :edit ++encoding=ibm866<CR>
    menu Encoding.CP1251  :edit ++encoding=cp1251<CR>
    menu Encoding.UCS-2LE :edit ++encoding=ucs-2le<CR>
    menu Encoding.Latin_1 :edit ++encoding=latin1<CR>
    noremap <C-F8> :emenu Encoding.<Tab>
    imap    <C-F8> <C-O><C-F8>

    " <Shift-F8> Change save encoding (convert)
    menu Encoding\ (Convert).UTF-8   :set fileencoding=utf-8<CR>
    menu Encoding\ (Convert).KOI8-R  :set fileencoding=koi8-r<CR>
    menu Encoding\ (Convert).CP866   :set fileencoding=ibm866<CR>
    menu Encoding\ (Convert).CP1251  :set fileencoding=cp1251<CR>
    menu Encoding\ (Convert).UCS-2LE :set fileencoding=ucs-2le<CR>
    menu Encoding\ (Convert).Latin_1 :set fileencoding=latin1<CR>
    noremap <S-F8> :emenu Encoding\ (Convert).<Tab>
    imap    <S-F8> <C-O><S-F8>

    " <Ctrl-Shift-F8> Toggle ASCII/binary mode (also affects EOL)
    menu File\ mode.ASCII  :set nobinary<CR>:set endofline<CR>
    menu File\ mode.Binary :set binary<CR>:set noendofline<CR>
    noremap <C-S-F8> :emenu File\ mode.<Tab>
    imap    <C-S-F8> <C-O><C-S-F8>

# WEB LINKS

    https://www.vim.org/scripts/script.php?script_id=6033

# COPYRIGHT AND LICENSE

    Copyright (C) 2022-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                            <neva_blyad@lovecri.es>

    vim-deadc0de is licensed under the Vim License.

    There are no restrictions on using or distributing an unmodified copy of the
    software. Parts of the software may also be distributed, but the license
    text must always be included. For modified versions a few restrictions
    apply. The license is GPL compatible, you may compile the software with GPL
    libraries and distribute it.

# IN LOVING MEMORY OF

Irina
Natalia
Daria
Daria

# AUTHORS

    НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                    <neva_blyad@lovecri.es>
    Invisible Light
