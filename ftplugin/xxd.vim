"    xxd.vim
"    Copyright (C) 2022-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
"                                            <neva_blyad@lovecri.es>
"
"    This program is free software: you can redistribute it and/or modify
"    it under the terms of the GNU General Public License as published by
"    the Free Software Foundation, either version 3 of the License, or
"    (at your option) any later version.
"
"    This program is distributed in the hope that it will be useful,
"    but WITHOUT ANY WARRANTY; without even the implied warranty of
"    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
"    GNU General Public License for more details.
"
"    You should have received a copy of the GNU General Public License
"    along with this program.  If not, see <https://www.gnu.org/licenses/>.

" UTF-8
scriptencoding utf-8

" Only do this when not done yet for this buffer
if exists("b:did_ftplugin")
    finish
endif
let b:did_ftplugin = 1

" Save compatibility options
let s:cpo_save = &cpo
set cpo&vim

" Set options
setlocal nobinary
setlocal endofline
setlocal fixendofline
setlocal nowrap

" Undo the stuff we changed
let b:undo_ftplugin = 'unlet! b:col b:mod5 b:len b:num b:col_hex b:col_ascii b:enc b:bin |
                     \ setlocal binary< endofline< fixendofline< wrap< |
                     \ syntax clear cur |
                     \ autocmd! * <buffer>'
let &cpo = s:cpo_save
unlet s:cpo_save
