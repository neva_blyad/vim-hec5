"    vim-deadc0de.vim
"    Copyright (C) 2022-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
"                                            <neva_blyad@lovecri.es>
"
"    This program is free software: you can redistribute it and/or modify
"    it under the terms of the GNU General Public License as published by
"    the Free Software Foundation, either version 3 of the License, or
"    (at your option) any later version.
"
"    This program is distributed in the hope that it will be useful,
"    but WITHOUT ANY WARRANTY; without even the implied warranty of
"    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
"    GNU General Public License for more details.
"
"    You should have received a copy of the GNU General Public License
"    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"
"  _____                                  ___             
" |  ___|__  _ __   _   _  ___  _   _    |_ _|_ __ __ _   
" | |_ / _ \| '__| | | | |/ _ \| | | |    | || '__/ _` |  
" |  _| (_) | |    | |_| | (_) | |_| |_   | || | | (_| |_ 
" |_|  \___/|_|     \__, |\___/ \__,_( ) |___|_|  \__,_( )
"                   |___/            |/                |/ 
"   __                                         _                     _   _ 
"  / _|_ __ ___  _ __ ___    _ __ ___  _   _  | |__   ___  __ _ _ __| |_| |
" | |_| '__/ _ \| '_ ` _ \  | '_ ` _ \| | | | | '_ \ / _ \/ _` | '__| __| |
" |  _| | | (_) | | | | | | | | | | | | |_| | | | | |  __/ (_| | |  | |_|_|
" |_| |_|  \___/|_| |_| |_| |_| |_| |_|\__, | |_| |_|\___|\__,_|_|   \__(_)
"                                      |___/                                
" 
" (Also for Natasha and Dasha; forever in my memory.)

" UTF-8
scriptencoding utf-8

" Do HEX-to-ASCII conversion (and vice versa) for all printable characters.
" (Only latin alphabet and few special characters work otherwise.)
"
" Enabled by default.
if !exists('g:deadc0de_ascii_extended')
    let g:deadc0de_ascii_extended = 1
endif

" HEX bytes case.
"
" Lowercase by default.
if !exists('g:deadc0de_hex_uppercase')
    let g:deadc0de_hex_uppercase = 0
endif

" Hex file protection.
" Disallow user to break the xxd file format.
"
" Enabled by default.
if !exists('g:deadc0de_protection')
    let g:deadc0de_protection = 1
endif

" Use optimized conversion to ASCII on load.
"
" Enabled by default.
if !exists('g:deadc0de_fast_conversion')
    let g:deadc0de_fast_conversion = 1
endif

" The currently edited byte
let b:col  = 0
let b:mod5 = 0
let b:len  = 0
let b:num  = 0

let b:col_hex   = 0
let b:col_ascii = 0

" Convert hex to ASCII.
"
" Each item in the parameter should have the 4-characters format, e. g. AH14.
" Item delimiter is space.
function! s:Hex2ASCII(hex)
    if b:enc == "utf-8"
        let l:cmd = "perl -e 'foreach (@ARGV)
                   \          {
                   \              my $tmpl = length == 2 ? q(H2) : q(H4);
                   \              my $out  = pack $tmpl, $_;
                   \
                   \              for (0, 1)
                   \              {
                   \                  my $ch = substr $out, $_, 1;
                   \                  print($ch ge q( ) && $ch le q(~) ? $ch : q(.));
                   \
                   \                  last if length $out == 1;
                   \              }
                   \          }' " . a:hex
    else
        let l:cmd = "perl -e 'use Text::Iconv;
                   \
                   \          my $iconv = Text::Iconv->new(qw(" . b:enc . ", utf-8));
                   \
                   \          foreach (@ARGV)
                   \          {
                   \              for my $idx (0, 2)
                   \              {
                   \                  my $hex = substr $_, $idx, 2;
                   \                  my $dec = hex $hex;
                   \                  my $ch  = pack q(S), $dec;
                   \
                   \                  my $conv_ch = $iconv->convert($ch);
                   \                  $conv_ch = substr $conv_ch, 0, -1;
                   \                  print($conv_ch =~ m/\\p{Print}/ ? $conv_ch : q(.));
                   \
                   \                  last if length == 2;
                   \              }
                   \          }' " . a:hex
    endif

    let l:ascii = system(l:cmd)
    return l:ascii
endfunction

" Convert ASCII to hex.
"
" Each item in the returned value should have the 4-characters format, e. g.
" AH14.
" Item delimiter is space.
function! s:ASCII2Hex(ascii)
    let l:ascii = escape(a:ascii, "'") 

    if b:enc == "utf-8"
        let l:cmd = "perl -e 'use Encode;
                   \
                   \          Encode::_utf8_on($ARGV[0]);
                   \          my $len = length $ARGV[0];
                   \
                   \          for (my $idx = 0; $idx < $len; $idx += 2)
                   \          {
                   \              my $in  = substr $ARGV[0], $idx, 2;
                   \              my $hex = unpack q(H4), $in;
                   \
                   \              print uc $hex, q( );
                   \          }' '" . l:ascii . "'"
    else
        let l:cmd = "perl -e 'use Encode;
                   \          use Text::Iconv;
                   \
                   \          my $iconv = Text::Iconv->new(qw(utf-8, " . b:enc . "));
                   \          Encode::_utf8_on($ARGV[0]);
                   \          my $len = length $ARGV[0];
                   \
                   \          for (my $idx = 0; $idx < $len; $idx++)
                   \          {
                   \              my $ch      = substr $ARGV[0], $idx, 1;
                   \              my $conv_ch = $iconv->convert($ch);
                   \              my $dec     = unpack q(C), $conv_ch;
                   \              my $hex     = sprintf q(%X), $dec;
                   \
                   \              print $hex, $idx % 2 == 0 ? q() : q( );
                   \          }' '" . l:ascii . "'"
    endif

    let l:hex = system(l:cmd)
    return l:hex
endfunction

" Fill buffer with hexdump of current data.
" (Uses this buffer or opens a new one.)
"
" Update ASCII block.
"
" Note. xxd converts only 7-bit characters of latin alphabet.
" This function do smarter, all printable characters from other one-byte
" encodings should be converted.
function! s:HexdumpFill()
    " Which conversion method should be use.
    " Convert the entire buffer at one or do it subsequently line by line.
    if g:deadc0de_fast_conversion
        " Use fast conversion
        let l:temp = system("mktemp")
        execute "write!" l:temp

        if b:enc == "utf-8"
            let l:cmd = "perl -e 'use re q(eval);
                       \
                       \          my $temp    = $ARGV[0];
                       \          my $flags   = $ARGV[1];
                       \          my $conv    = $ARGV[2];
                       \
                       \          my $pattern = q((?:^[0-9a-fA-F]{8}: )((?:[0-9a-fA-F]{4} )*?(?:[0-9a-fA-F]{2}){1,2})(??{q( ) x (41 - length $1)})\\K(?:.{1,16}));
                       \          my $out     = q();
                       \
                       \          sub hex2ascii
                       \          {
                       \              my $ascii = q();
                       \              my @arr = split / /, $_[0];
                       \
                       \              foreach (@arr[0..$#arr-1])
                       \              {
                       \                  my $tmpl = q(H4);
                       \                  my $out  = pack $tmpl, $_;
                       \
                       \                  for (0, 1)
                       \                  {
                       \                      my $ch = substr $out, $_, 1;
                       \                      $ascii .= $ch ge q( ) && $ch le q(~) ? $ch : q(.);
                       \                  }
                       \              }
                       \
                       \              $_ = $arr[-1];
                       \
                       \              my $tmpl = length == 2 ? q(H2) : q(H4);
                       \              my $out  = pack $tmpl, $_;
                       \
                       \              for (0, 1)
                       \              {
                       \                  my $ch = substr $out, $_, 1;
                       \                  $ascii .= $ch ge q( ) && $ch le q(~) ? $ch : q(.);
                       \
                       \                  last if length $out == 1;
                       \              }
                       \
                       \              return $ascii;
                       \          }
                       \
                       \          open TEMP, qq(xxd $flags $temp |);
                       \
                       \          if ($conv)
                       \          {
                       \              foreach (<TEMP>)
                       \              {
                       \                  s/$pattern/hex2ascii($1)/e;
                       \                  $out .= $_;
                       \              }
                       \          }
                       \          else
                       \          {
                       \              foreach (<TEMP>)
                       \              {
                       \                  $out .= $_;
                       \              }
                       \          }
                       \
                       \          close TEMP;
                       \          open  TEMP, qq(>$temp);
                       \          print TEMP $out;
                       \          close TEMP;' '" . l:temp . "' " . (g:deadc0de_hex_uppercase ? "-u" : "") . " " . g:deadc0de_ascii_extended
        else
            let l:cmd = "perl -e 'use Text::Iconv;
                       \          use re q(eval);
                       \
                       \          my $temp    = $ARGV[0];
                       \          my $flags   = $ARGV[1];
                       \          my $conv    = $ARGV[2];
                       \
                       \          my $pattern = q((?:^[0-9a-fA-F]{8}: )((?:[0-9a-fA-F]{4} )*?(?:[0-9a-fA-F]{2}){1,2})(??{q( ) x (41 - length $1)})\\K(?:.{1,16}));
                       \          my $out     = q();
                       \
                       \          sub hex2ascii
                       \          {
                       \              my $ascii = q();
                       \              my @arr = split / /, $_[0];
                       \
                       \              foreach (@arr[0..$#arr-1])
                       \              {
                       \                  for my $idx (0, 2)
                       \                  {
                       \                      my $hex = substr $_, $idx, 2;
                       \                      my $dec = hex $hex;
                       \                      my $ch  = pack q(S), $dec;
                       \                      my $ch_ = substr $ch, 0, -1;
                       \
                       \                      $ascii .= $ch_ =~ m/\\p{Print}/ ? $ch_ : q(.);
                       \                  }
                       \              }
                       \
                       \              $_ = $arr[-1];
                       \
                       \              for my $idx (0, 2)
                       \              {
                       \                  my $hex = substr $_, $idx, 2;
                       \                  my $dec = hex $hex;
                       \                  my $ch  = pack q(S), $dec;
                       \                  my $ch_ = substr $ch, 0, -1;
                       \
                       \                  $ascii .= $ch_ =~ m/\\p{Print}/ ? $ch_ : q(.);
                       \                  last if length == 2;
                       \              }
                       \
                       \              return $ascii;
                       \          }
                       \
                       \          open TEMP, qq(xxd $flags $temp |);
                       \
                       \          if ($conv)
                       \          {
                       \              foreach (<TEMP>)
                       \              {
                       \                  s/$pattern/hex2ascii($1)/e;
                       \                  $out .= $_;
                       \              }
                       \          }
                       \          else
                       \          {
                       \              foreach (<TEMP>)
                       \              {
                       \                  $out .= $_;
                       \              }
                       \          }
                       \
                       \          close TEMP;
                       \          open  TEMP, qq(>$temp);
                       \          print TEMP $out;
                       \          close TEMP;' '" . l:temp . "' " . (g:deadc0de_hex_uppercase ? "-u" : "") . " " . g:deadc0de_ascii_extended
        endif

        let l:res = system(l:cmd)
        let l:enc = b:enc
        let l:bin = b:bin
        execute "enew"
        execute "edit!" " ++encoding=" . l:enc l:temp
        let b:enc = l:enc
        let b:bin = l:bin
        call setpos(".", [ 0, 1, 11, 0 ])
    else
        " Use slow conversion
        if g:deadc0de_hex_uppercase
            %!xxd -u
        else
            %!xxd
        endif

        if g:deadc0de_ascii_extended
            let line     = 1
            let max_line = line("$")

            while line <= max_line
                call s:LineUpd(line, v:false)
                let line += 1
            endwhile
            call setpos(".", [ 0, 1, 11, 0 ])
        endif
    endif
endfunction

" Highlight byte under the cursor in HEX block and the corresponding character
" in ASCII block.
"
" Do vice versa when editing in ASCII block.
function! s:CurHighlight()
    let pos      = getcurpos()
    let l:line   = pos[1]
    let b:col    = pos[4]
    let max_line = line("$")

    if  b:col == 2147483647 ||
    \  (b:col == 67 && l:line == max_line) " This quirk fixes Vim 8.2 (and, possibly, other versions) bug in getcurpos()
        normal h
        normal l

        let pos   = getcurpos()
        let b:col = pos[4]
    endif

    let set_syntax = v:false
    let b:mod5     = b:col % 5

    if l:line == max_line
        let l:line_str = getline(l:line)
        let l:pattern  = '^\x\{8}: \(\%(\x\{4} \)\{-}\%(\x\{2}\)\{1,2}\) \{-1,}.\{1,16}$'
        let l:matches  = matchlist(l:line_str, l:pattern)

        if l:matches == []
            return
        endif

        let b:len = len(l:matches[1])
        let b:num = b:len - (b:len / 4 - 1)
    else
        let b:len = 39
        let b:num = 16
    endif

    if b:col >= 11 && b:col <= 11 + b:len - 1 && b:mod5 != 0
        " We are in the HEX block.
        "
        " Find character in ASCII block.
        let set_syntax  = v:true
        let b:col_hex   = b:col - (b:mod5 == 2 || b:mod5 == 4)
        let b:col_ascii = 2 * ((b:col_hex - 11) / 5) + 52 + (b:mod5 == 3 || b:mod5 == 4)
    elseif b:col >= 52 && b:col <= 52 + b:num - 1
        " We are in the ASCII block.
        "
        " Find byte in HEX block.
        let set_syntax  = v:true
        let b:col_ascii = b:col
        let b:col_hex   = (b:col_ascii - 52) / 2 * 5 + 11 + 2 * (b:col_ascii % 2)
    endif

    syntax clear cur
    syntax clear xxdAscii
    if set_syntax
        let l:pattern = '"\%' . l:line . 'l\%(\%' . b:col_hex . 'c\x\x\|\%' . b:col_ascii . 'v.\)"'
        execute 'syntax match cur ' . l:pattern
        highlight default link cur Identifier
    endif
    syntax match xxdAscii "  .\{,16\}\r\=$"hs=s+2 contains=xxdDot,cur
endfunction

" Update ASCII representation of the currently selected byte.
" Update HEX representation of the currently selected character.
function! s:LineUpd(line, restore_pos)
    let l:line_str = getline(a:line)
    let l:pattern  = '^\(\x\{8}\): \(\%(\x\{4} \)\{-}\%(\x\{2}\)\{1,2}\) \{-1,}\(.\{1,16}\)$'
    let l:matches  = matchlist(l:line_str, l:pattern)

    if l:matches == []
        if g:deadc0de_protection
            undo
            stopinsert
        endif
        return
    endif

    let l:pos  = getpos(".")
    let l:addr = l:matches[1]

    if l:pos[2] <= 51
        " We are in the HEX block.
        "
        " Convert it to ASCII.
        let l:hex    = l:matches[2]
        let l:spaces = repeat(" ", 40 - len(l:hex))
        let l:ascii  = s:Hex2ASCII(l:hex)
    else
        " We are in the ASCII block.
        "
        " Convert it to HEX.
        let l:hex_len   = len(l:matches[2])
        let l:ascii     = l:matches[3]
        let l:col       = l:hex_len + 10
        let l:mod5      = l:col % 5
        let l:col_hex   = l:col - (l:mod5 == 2 || l:mod5 == 4)
        let l:col_ascii = 2 * ((l:col_hex - 11) / 5) + 52 + (l:mod5 == 3 || l:mod5 == 4)
        let l:ascii_len = l:col_ascii - 51

        let l:ascii     = strcharpart(l:ascii, strchars(l:ascii)  - l:ascii_len)
        let l:pattern   = '\.'
        let l:matches2  = matchlist(l:ascii, l:pattern)

        if l:matches2 != []
            if g:deadc0de_protection
                undo
                stopinsert
            endif
            return
        endif

        let l:hex    = s:ASCII2Hex(l:ascii)
        let l:spaces = repeat(" ", 40 - len(l:hex))
    endif

    let l:str = l:addr . ": " . l:hex . " " . l:spaces . l:ascii
    call setline(a:line, l:str)

    if a:restore_pos
        call setpos(".", l:pos)
    endif
endfunction

" Toggle hex file protection.
" This protection is also controlled by g:deadc0de_protection.
function! <SID>ProtectionToggle()
    if g:deadc0de_protection
        let g:deadc0de_protection = 0
    else
        let g:deadc0de_protection = 1
    endif
endfunction

" Cycle between HEX and ASCII blocks
function! <SID>BlocksCycle()
    if b:col >= 11 && b:col <= 11 + b:len - 1 && b:mod5 != 0
        " We are in the HEX block.
        "
        " Go to ASCII.
        execute 'normal ' . b:col_ascii . '|'
    elseif b:col >= 52 && b:col <= 52 + b:num - 1
        " We are in the ASCII block.
        "
        " Go to HEX.
        execute 'normal ' . b:col_hex . '|'
    endif
endfunction

" Edit binary using hex editor
command! HexConvert call <SID>HexConvert()

function! <SID>HexConvert()
    " Check filetype
    if &filetype == "xxd"
        " We're already editing xxd file.
        " Reset buffer options and convert it back.
        if b:bin
            setlocal filetype=
            setlocal fenc=
            setlocal binary
            setlocal endofline
            setlocal nofixendofline
        else
            setlocal filetype=
            setlocal nobinary
            setlocal endofline
            setlocal fixendofline
        endif

        %!xxd -r
    else
        " We're editing not-xxd file.
        " Remember buffer parameters.
        " Do conversion to ASCII.
        let b:enc = &fenc
        let b:bin = &binary

        call s:HexdumpFill()

        setlocal filetype=xxd
        syntax match cur "$"

        " Add mappings, unless the user didn't want this
        if !exists("no_plugin_maps")
            nnoremap <Plug>hexprotectiontoggle :call <SID>ProtectionToggle()<CR>
            nnoremap <Plug>hexblockscycle      :call <SID>BlocksCycle()<CR>
        endif

        " Add autocommands
        autocmd CursorMoved,CursorMovedI <buffer> :call s:CurHighlight()
        if g:deadc0de_ascii_extended
            autocmd TextChanged,TextChangedI,TextChangedP <buffer> :call s:LineUpd(line("."), v:true)
        endif
    endif
endfunction
